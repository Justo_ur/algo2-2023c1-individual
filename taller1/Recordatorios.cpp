#include <iostream>
#include <string>
#include <list>
#include <algorithm>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha{
  public:
    Fecha(int mes, int dia);
    int m();
    int d();
    void incrementar_dia();
    bool operator==(Fecha o);

    private:
        int mes_;
        int dia_;
        bool es_fecha_valida(int mes, int dia);
        //Completar miembros internos
    };


Fecha::Fecha(int mes, int dia){
    if (es_fecha_valida(mes, dia)) {
        mes_ = mes;
        dia_ = dia;
    } else {
        cerr << "Error: fecha invalida" << endl;
        exit(1);
    }
}

int Fecha::m(){
    return mes_;
}

int Fecha::d(){
    return dia_;
}

bool Fecha::es_fecha_valida(int mes, int dia) {
    if (mes < 1 || mes > 12) {
        return false;
    }
    if (dia < 1 || dia > 31) {
        return false;
    }
    if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
        if (dia > 30) {
            return false;
        }
    }
}


ostream& operator<<(ostream& os, Fecha f) {
    os << "Fecha(" << f.m() << ", " << f.d() << ")";
    return os;
}











bool Fecha::operator==(Fecha o) {
    return mes_ == o.mes_ && dia_ == o.dia_;
}


void Fecha::incrementar_dia(){
    if(dia_<dias_en_mes(mes_)){
        dia_=dia_+1;

    }
}





// Ejercicio 11, 12

class Horario {
    public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        bool operator<(Horario h);

private:
        hora_;
        min_;

};

Horario::Horario(uint hora, uint min){
    min_ = min;
    hora_ = hora;
}

uint Horario::min(){
    return min_
}

uint Horario::hora(){
    return hora_
}

bool Horario::operator<(Horario h) {
    if (hora_ < h.hora_){
        return true;

    }
    else if (hora_ == h.hora_){
        return min_ < h.min();
    }
    else{
        return false;
    }
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}




// Ejercicio 13

class Recordatorio {
    public:
        Recordatoio(string msj,Fecha fecha, Horario hora);
        friend ostream& operator<<(ostream& os, Recordatorio r);

    private:
        Fecha fecha_;
        Horario horario_;
        string mensaje_;
};

Recordatorio::Recordatorio(string msj, Fecha fecha,Horario hora){
    mensaje_ = msj;
    horario_ = hora;
    fecha_ = fecha;

}


//El uso de la palabra clave friend permite evitar tener que definir metodos publicos innecesarios y
// proporciona un acceso directo a los miembros privados de la clase Recordatorio.

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.mensaje_ << " @ " << r.fecha_ << " " << r.horario_;
    return os;
}
// Ejercicio 14

class Agenda {
    public:
        Agenda(Fecha fecha_inicial);
        void agregar_recordatorio(Recordatorio rec);
        void incrementar_dia();
        list<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();
        friend ostream& operator<<(ostream& os, Agenda a);



    private:
        Fecha fecha_actual_;
        list<Recordatorio> recordatorios_;

};

Agenda::Agenda(Fecha fecha_inicial)
        : fecha_actual_(fecha_inicial) {}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    recordatorios_.push_back(rec);
}

void Agenda::incrementar_dia() {
    fecha_actual_.incrementar_dia();
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> recordatorios_hoy;
    for (const auto& rec : recordatorios_) {
        if (rec.getFecha() == fecha_actual_) {
            recordatorios_hoy.push_back(rec);
        }
    }
    return recordatorios_hoy;
}

Fecha Agenda::hoy() {
    return fecha_actual_;
}

}

ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() << endl << "=====" << endl;
    list<Recordatorio> recordatorios_hoy = a.recordatorios_de_hoy();
    for (const auto& rec : recordatorios_hoy) {
        os << rec << endl;
    }
    return os;
}





